<? // include file ?>

<?
$APPLICATION->IncludeFile(
	SITE_DIR."include/random.php",
	Array(),
	Array("MODE"=>"html")
);
?>

<?// main include?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/footer/counter.php"
	)
);?>

<?// main include with hide?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/footer/basket-fixed.php"
	),
	false,
	array('HIDE_ICONS' => 'Y')
);?>

<?// main include line?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_logo.php"), false);?>

<?// Страница только вызывается, не открывается отдельно?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?//Для страницы вызываемой AJAX?>
<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?// Путь до шаблона сайта(текущего)?>
<?=SITE_TEMPLATE_PATH?>

<?// Директория сайта?>
<?=SITE_DIR?>

<?// Показать title?>
<?$APPLICATION->ShowTitle()?>


<?// Показать h1?>
<?$APPLICATION->ShowTitle(false);?>


<?
// D7 подключение скриптов
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/fix.js");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/styles/fix.css");
Asset::getInstance()->addString("<link href='http://fonts.googleapis.com/css?family=PT+Sans:400&subset=cyrillic' rel='stylesheet' type='text/css'>");
?>

<?// Если на главной;?>
<?if ($APPLICATION->GetCurPage() === '/'):?>

<?// Если в директории about?>
<?if(CSite::InDir('/about/'))?>

<?$file = CFile::ResizeImageGet($uInfo['PERSONAL_PHOTO'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>

<?$file = CFile::GetPath($arElement["PREVIEW_PICTURE"];?>